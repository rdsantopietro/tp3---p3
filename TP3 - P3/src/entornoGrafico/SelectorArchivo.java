package entornoGrafico;
import java.io.*;
import java.io.ObjectInputStream.GetField;
import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.filechooser.*;

public class SelectorArchivo extends JFrame {

    private JTextArea log;
    private JFileChooser fc = new JFileChooser();

    private String newline = System.getProperty("line.separator");
    private String archivo=" ";
   

    public SelectorArchivo() {
        super("Seleccione Archivo");
        setResizable(false);
        setAlwaysOnTop(true);
        this.setSize(new Dimension(488, 206));


        JButton openButton = new JButton("Open", new ImageIcon("images/open.gif"));
        openButton.setBounds(45, 85, 131, 23);
        openButton.addActionListener(new OpenListener());
        fc.setBounds(100, 100, 594, 375);
        
        JPanel buttonPanel = new JPanel();
        buttonPanel.setBounds(0, 0, 434, 167);
        buttonPanel.setLayout(null);
        buttonPanel.add(openButton);

        log = new JTextArea(5,20);
        log.setMargin(new Insets(5,5,5,5));
        JScrollPane logScrollPane = new JScrollPane(log);

        Container contentPane = getContentPane();
        getContentPane().setLayout(null);
        contentPane.add(buttonPanel);
        
        JButton btnCerrar = new JButton("Cerrar", null);
        btnCerrar.addActionListener(new ActionListener() {
        	public void actionPerformed(ActionEvent e) {
        		dispose();
        	}
        });
        btnCerrar.setBounds(222, 85, 131, 23);
        buttonPanel.add(btnCerrar);
        
        JLabel lblNewLabel = new JLabel("1- Presionar \"Open\" y elegir archivo\r\n");
        lblNewLabel.setBounds(45, 11, 210, 23);
        buttonPanel.add(lblNewLabel);
        
        JLabel lblNewLabel_1 = new JLabel("2- Presionar \"Cerrar\" una vez elegido el mismo");
        lblNewLabel_1.setBounds(45, 41, 262, 33);
        buttonPanel.add(lblNewLabel_1);
      //  contentPane.add(logScrollPane, BorderLayout.CENTER);
    }

    private class OpenListener implements ActionListener {
        public void actionPerformed(ActionEvent e) {
            int returnVal = fc.showOpenDialog(SelectorArchivo.this);

            if (returnVal == JFileChooser.APPROVE_OPTION) {
                File file = fc.getSelectedFile();
                //this is where a real application would open the file.
                log.append("Opening: " + file.getName() + "." + newline);
                archivo=file.getAbsolutePath();
            }
        }
    }

    public static void main(String s[]) {
        JFrame frame = new SelectorArchivo();

        frame.addWindowListener(new WindowAdapter() {
            public void windowClosing(WindowEvent e) {System.exit(0);}
        });

        frame.pack();
        frame.setVisible(true);
        //System.out.println(frame.get);
    }
    
    public String getFile() {
    	return archivo;
    }
}
