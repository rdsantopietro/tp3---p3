package entornoGrafico;

import java.awt.EventQueue;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.SwingUtilities;
import javax.swing.SwingWorker;
import javax.swing.UIManager;
import javax.swing.filechooser.FileFilter;
import javax.swing.filechooser.FileView;
import javax.swing.plaf.FileChooserUI;
import javax.swing.plaf.ProgressBarUI;
import javax.naming.LimitExceededException;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;
import javax.swing.JFileChooser;

import java.awt.event.ActionListener;
import java.io.File;
import java.awt.event.ActionEvent;
import javax.swing.JProgressBar;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.Insets;
import java.awt.Button;
import javax.swing.JComboBox;

public class Ventana {
	private JFrame frame;
	private JProgressBar barraProgreso;
	private JButton btnProcesar;
	private SelectorArchivo selectorNomina = new SelectorArchivo();
	private SelectorArchivo selectorRelaciones = new SelectorArchivo();
	private JButton btnProcesar_1;
	private JButton limpiarMemoria;
	private Resultados resultados = new Resultados();
	private JButton buscoEquipo;
	private JComboBox comboLideres;
	private JComboBox comboArquitectos;
	private JComboBox comboProgramadores;
	private JComboBox comboTesters;
	private UIManager.LookAndFeelInfo[] temas = UIManager.getInstalledLookAndFeels();
	private JButton btnResultados;



	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Ventana window = new Ventana();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public Ventana() {
		initialize();
	}

	public String[] nombresLookAndFeel() {
		String[] nombres = new String[temas.length];
		for (int i = 0; i < temas.length; i++) {
			nombres[i] = temas[i].getName();
		}
		return nombres;
	}
	public JButton getBtnResultados() {
		return btnResultados;
	}
	public JProgressBar getBarraProgreso() {
		return barraProgreso;
	}

	public void setBarraProgreso(JProgressBar barraProgreso) {
		this.barraProgreso = barraProgreso;
	}
	public void muestroResultados(String[][] objects) {
		resultados.llenoMatriz(objects);
		resultados.getFrame().setVisible(true);
		System.out.println("wntre");
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setResizable(false);
		frame.setBounds(100, 100, 594, 375);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);

		JLabel lblResultado = new JLabel("Seleccione Relaciones");
		lblResultado.setFont(new Font("Tahoma", Font.BOLD, 14));
		lblResultado.setBounds(35, 69, 132, 20);
		frame.getContentPane().add(lblResultado);

		JLabel lblFibonacciDe = new JLabel("Seleccione Nomina");
		lblFibonacciDe.setFont(new Font("Tahoma", Font.BOLD, 14));
		lblFibonacciDe.setBounds(35, 31, 150, 27);
		frame.getContentPane().add(lblFibonacciDe);

		buscoEquipo = new JButton("Buscar Equipo");
		buscoEquipo.setFont(new Font("Tahoma", Font.PLAIN, 11));
		buscoEquipo.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {

			}
		});
		buscoEquipo.setBounds(35, 195, 161, 41);
		frame.getContentPane().add(buscoEquipo);
		barraProgreso = new JProgressBar();
		barraProgreso.setBounds(35, 247, 511, 27);
		frame.getContentPane().add(barraProgreso);

		JButton btnAbrir = new JButton("1 -Abrir");
		btnAbrir.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				selectorNomina.setVisible(true);
			}
		});
		btnAbrir.setFont(new Font("Tahoma", Font.PLAIN, 11));
		btnAbrir.setBounds(183, 33, 119, 27);
		frame.getContentPane().add(btnAbrir);

		btnProcesar = new JButton("2 - Procesar Nomina");
		btnProcesar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {

			}
		});
		btnProcesar.setFont(new Font("Tahoma", Font.PLAIN, 11));
		btnProcesar.setBounds(326, 33, 150, 27);
		frame.getContentPane().add(btnProcesar);

		JButton btnabrir = new JButton("3 -Abrir");
		btnabrir.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				selectorRelaciones.setVisible(true);

			}
		});
		btnabrir.setFont(new Font("Tahoma", Font.PLAIN, 11));
		btnabrir.setBounds(183, 68, 119, 27);
		frame.getContentPane().add(btnabrir);

		btnProcesar_1 = new JButton("4 - Procesar Relaciones");
		btnProcesar_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
			}
		});
		btnProcesar_1.setFont(new Font("Tahoma", Font.PLAIN, 11));
		btnProcesar_1.setBounds(326, 70, 150, 27);
		frame.getContentPane().add(btnProcesar_1);

		JLabel lblLideres = new JLabel("Lideres");
		lblLideres.setFont(new Font("Tahoma", Font.BOLD, 14));
		lblLideres.setBounds(35, 115, 132, 20);
		frame.getContentPane().add(lblLideres);

		JLabel lblArquitectos = new JLabel("Arquitectos");
		lblArquitectos.setFont(new Font("Tahoma", Font.BOLD, 14));
		lblArquitectos.setBounds(120, 115, 132, 20);
		frame.getContentPane().add(lblArquitectos);

		JLabel lblProgramadores = new JLabel("Programadores");
		lblProgramadores.setFont(new Font("Tahoma", Font.BOLD, 14));
		lblProgramadores.setBounds(224, 115, 132, 20);
		frame.getContentPane().add(lblProgramadores);

		JLabel lblTesters = new JLabel("Testers");
		lblTesters.setFont(new Font("Tahoma", Font.BOLD, 14));
		lblTesters.setBounds(366, 115, 132, 20);
		frame.getContentPane().add(lblTesters);

		comboLideres = new JComboBox();

		comboLideres.setBounds(35, 146, 68, 20);
		frame.getContentPane().add(comboLideres);

		comboArquitectos = new JComboBox();
		comboArquitectos.setBounds(120, 146, 68, 20);
		frame.getContentPane().add(comboArquitectos);

		comboProgramadores = new JComboBox();
		comboProgramadores.setBounds(234, 146, 68, 20);
		frame.getContentPane().add(comboProgramadores);

		comboTesters = new JComboBox();
		comboTesters.setBounds(361, 146, 68, 20);
		llenoCombo(comboArquitectos);
		llenoCombo(comboLideres);
		llenoCombo(comboProgramadores);
		llenoCombo(comboTesters);
		frame.getContentPane().add(comboTesters);

		limpiarMemoria = new JButton("Limpiar Memoria");
		limpiarMemoria.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
			}
		});
		limpiarMemoria.setFont(new Font("Tahoma", Font.PLAIN, 11));
		limpiarMemoria.setBounds(397, 195, 161, 41);
		frame.getContentPane().add(limpiarMemoria);

		btnResultados = new JButton("Resultados!");
		btnResultados.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
			}
		});
		btnResultados.setFont(new Font("Tahoma", Font.PLAIN, 11));
		btnResultados.setBounds(224, 195, 161, 41);
		frame.getContentPane().add(btnResultados);
		btnResultados.setVisible(false);

		frame.setVisible(true);
	}

	public void escuchoProcesarNomina(ActionListener escuchaBotonProcesar) {
		btnProcesar.addActionListener(escuchaBotonProcesar);

	}

	public void escuchoLimpiarMemoria(ActionListener escuchoLimpiarMemoria) {
		limpiarMemoria.addActionListener(escuchoLimpiarMemoria);

	}

	public void escuchoProcesarRelaciones(ActionListener escuchaBotonProcesar) {
		btnProcesar_1.addActionListener(escuchaBotonProcesar);

	}

	public String archivoNomina() {
		return selectorNomina.getFile();
	}

	public String archivoRelaciones() {
		return selectorRelaciones.getFile();
	}

	public void llenoCombo(JComboBox combo) {
		for (int x = 0; x < 1001; x++)
			combo.addItem(String.valueOf(x));
	}

	public void escuchoBuscoEquipo(ActionListener buscoEquipoListener) {

		buscoEquipo.addActionListener(buscoEquipoListener);
	}

	public void escuchoResultados(ActionListener escuchoResultados) {

		btnResultados.addActionListener(escuchoResultados);
	}

	public int lideres() {
		return Integer.valueOf((String) comboLideres.getSelectedItem());
	}

	public int arquitectos() {
		return Integer.valueOf((String) comboArquitectos.getSelectedItem());
	}

	public int programadores() {
		return Integer.valueOf((String) comboProgramadores.getSelectedItem());
	}

	public int testers() {
		return Integer.valueOf((String) comboTesters.getSelectedItem());
	}

	public void setProceso(int clique) {
		barraProgreso.setValue(clique);
	}

	public JProgressBar getBarraProgresos() {
		// TODO Auto-generated method stub
		return barraProgreso;
	}

	public void alertaExcepcion(Exception e1) {
		JOptionPane.showMessageDialog(frame, "Atencion, " + e1.getMessage() + ". ", "ATENCION!",
				JOptionPane.WARNING_MESSAGE);

	}

	public void alerta(String e1) {
		JOptionPane.showMessageDialog(frame, e1, "ATENCION!", JOptionPane.WARNING_MESSAGE);

	}
}