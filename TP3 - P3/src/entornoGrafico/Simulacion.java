package entornoGrafico;

import java.util.concurrent.ExecutionException;
import javax.swing.JProgressBar;
import javax.swing.JTextField;
import javax.swing.SwingWorker;

public class Simulacion extends SwingWorker<Long, Long>
{
	private int _n; 
	private JTextField resultJTextField; 
	private JProgressBar barraProgreso;
	
	public Simulacion( JProgressBar barra, int numero)
	{
		resultJTextField=new JTextField();
		barraProgreso = barra;	
		_n = numero;
	}
	
	@Override
	protected Long doInBackground() throws Exception 
	{
		barraProgreso.setIndeterminate(true);
		long nthFib = 0;
		return nthFib;
	}
	
	@Override
	public void done()
	{
	   // get el resultado de doInBackground y lo mostramos, si es que no nos cancelaron
	   if (this.isCancelled()==false)
	   {
		   resultJTextField.setText("procesando");
		   barraProgreso.setIndeterminate(false);
	   } 
	} 
}