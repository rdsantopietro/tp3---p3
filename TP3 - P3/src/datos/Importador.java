package datos;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import negocio.*;

public class Importador {

	public ArrayList<Empleado> cargoEmpleados(String archivo) {
		String csvFile = archivo;
		ArrayList <Empleado> empleados= new ArrayList<Empleado>();
		BufferedReader br = null;
		String line = "";
		String cvsSplitBy = ",";
		try {
			br = new BufferedReader(new FileReader(csvFile));
			int x=0;
			while ((line = br.readLine()) != null ) {
				String[] datos = line.split(cvsSplitBy);
				Empleado e = new Empleado(datos[0], Rol.valueOf(datos[1].toUpperCase()), x);
				empleados.add(e);
				x++;
			}
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			if (br != null) {
				try {
					br.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
		
		return empleados;
	}
	
	public void cargoMalasRelaciones(NominaEmpleados nomina, String archivo) {
		String csvFile = archivo;
		BufferedReader br = null;
		String line = "";
		String cvsSplitBy = ",";
		try {
			br = new BufferedReader(new FileReader(csvFile));
			while ((line = br.readLine()) != null ) {
				String[] datos = line.split(cvsSplitBy);
				String e1=datos[0];
				String e2=datos[1];
				nomina.agregoIncompatibilidad(nomina.buscoPorNombre(e1).get(), nomina.buscoPorNombre(e2).get());
			}
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			if (br != null) {
				try {
					br.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
	}
}
