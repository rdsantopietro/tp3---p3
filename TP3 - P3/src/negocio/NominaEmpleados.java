package negocio;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;


public class NominaEmpleados {
	private ArrayList <Empleado> empleados;
	private Grafo grafo;
	
	public NominaEmpleados(ArrayList<Empleado> e ) {
		this.empleados=e ;
		grafo= new Grafo(cantidadEmpleados());
	}
	
	public void agregoIncompatibilidad(Empleado e1, Empleado e2) {
		existeEmpleado(e1);
		existeEmpleado(e2);
		e1.agregoincompatible(e2);
		e2.agregoincompatible(e1);
		System.out.println("a " + e1.getId() +" le quito " + e2.getId());
		grafo.borrarArista(e1.getId(), e2.getId());
		System.out.println(grafo.existeArista(e1.getId(), e2.getId()));
		
	}
	
	public int cantidadEmpleados() {
		return empleados.size();
	}
	
	public Optional <Empleado>buscoPorNombre(String nombre) {
		Optional <Empleado> empleado=empleados.stream()
				.filter(x->x.getNombre()
                .equals(nombre))
				.findFirst();
		return  empleado;
	}
	
	public Empleado buscoPorId(int id) {
		verificoId(id);
		return empleados.get(id);
	}
	
	private void existeEmpleado(Empleado e)
	{
		verificoId(e.getId());
	}
	
	private void verificoId(int id)
	{
		if( id<0 || id>=cantidadEmpleados() )
			throw new IllegalArgumentException("No existe el ID del empleado");
	}
	

	public void imprimoGrafo() {
		grafo.imprimoGrafo();
	}
	
	public ArrayList<Empleado> getEmpleados(){
		return empleados;
	}
	
	public int cantidadLideres() {
		return (int) empleados.parallelStream()
				.filter(e -> e.getRol().equals(Rol.LIDER)).count();
	}
	public int cantidadArquitectos() {
		return (int) empleados.parallelStream()
				.filter(e -> e.getRol().equals(Rol.ARQUITECTO)).count();
	}
	public int cantidadProgramadores() {
		return (int) empleados.parallelStream()
				.filter(e -> e.getRol().equals(Rol.PROGRAMADOR)).count();
	}
	public int cantidadTesters() {
		return (int) empleados.parallelStream()
				.filter(e -> e.getRol().equals(Rol.TESTER)).count();
	}
	
	public void setEmpleados(ArrayList <Empleado> e) {
		empleados=e;
	}
	
	public List<Empleado> empleados(){
		return empleados;
	}
	
	public Grafo getGrafo() {
		return grafo;
	}
	

	
}
