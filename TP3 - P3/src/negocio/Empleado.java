package negocio;

import java.util.HashSet;
import java.util.Set;

public class Empleado {
	
	private String nombre;
	private Rol rol;
	private Set<Empleado> incompatibles;
	private int id;
	
	public Empleado(String nombre,  Rol rol, int id) {
		this.nombre = nombre;
		this.rol = rol;
		this.id = id;
		incompatibles = new HashSet<Empleado>();
	}
	
	public void agregoincompatible(Empleado e) {
		incompatibles.add(e);
	}
	
	public String getNombre() {
		return nombre;
	}
	
	public int getId() {
		return id;
	}
	
	public boolean equals(Empleado otro) {
		return this.nombre.equals(otro.getNombre()) && this.id==otro.getId() && this.rol.equals(otro.getRol());
	}

	public Rol getRol() {
		return rol;
	}
	
	@Override
	public String toString() {
		return  "Nombre: "+nombre + ". Rol: " + rol.toString()+ ". ID: "+id;
	}
	
}
