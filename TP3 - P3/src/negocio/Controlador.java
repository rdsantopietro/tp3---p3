package negocio;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Set;
import datos.*;
import entornoGrafico.Ventana;

public class Controlador {
	private Ventana ventana;
	private NominaEmpleados nomina;
	private Importador importador;
	private Necesidad necesidad;
	private Proyecto proyecto;
	private Runtime garbage = Runtime.getRuntime();

	public Controlador() {
		ventana = new Ventana();
		/*BOTONES*/
		ventana.escuchoProcesarNomina(new cargaProcesarNominaListener());
		ventana.escuchoProcesarRelaciones(new cargoProcesarRelacionesListener());
		ventana.escuchoBuscoEquipo(new buscoEquipoListener());
		ventana.escuchoLimpiarMemoria(new limpiarMemoriaListener());
		ventana.escuchoResultados(new resultadosListener());

	}

	public static void main(String[] args) {
		@SuppressWarnings(value = { "unused" })
		Controlador controlador = new Controlador();
	}

	class cargaProcesarNominaListener implements ActionListener {
		public void actionPerformed(ActionEvent e) {
			try {
				importador = new Importador();
				nomina = new NominaEmpleados(importador.cargoEmpleados(ventana.archivoNomina()));
				nomina.empleados().forEach(x -> System.out.println(x.getNombre()));

			} catch (NumberFormatException ex) {

			}

		}
	}

	class cargoProcesarRelacionesListener implements ActionListener {
		public void actionPerformed(ActionEvent e) {
			try {
				importador.cargoMalasRelaciones(nomina, ventana.archivoRelaciones());
				System.out.println(nomina.cantidadLideres());

			} catch (NumberFormatException ex) {
			}
		}
	}

	class buscoEquipoListener implements ActionListener {
		public void actionPerformed(ActionEvent e) {
			try {
				necesidad = new Necesidad(ventana.lideres(), ventana.arquitectos(), ventana.programadores(),
				ventana.testers());
				proyecto = new Proyecto(nomina, necesidad);
				proyecto.setBtnResultados(ventana.getBtnResultados());
				proyecto.tengoEmpleadosPorPuesto();
				proyecto.execute();

			} catch (NumberFormatException ex) {

			} catch (Exception e1) {
				ventana.alertaExcepcion(e1);
				proyecto.execute();
			}
		}
	}

	class limpiarMemoriaListener implements ActionListener {
		@Override
		public void actionPerformed(ActionEvent arg0) {
			try {
				ventana.alerta(pasarGarbageCollector());
			} catch (Exception e) {

			}
		}
	}

	class resultadosListener implements ActionListener {
		@Override
		public void actionPerformed(ActionEvent arg0) {
			try {
				muestroResultado(proyecto.getSolucion());
			} catch (Exception e) {
			}
		}
	}

	public String pasarGarbageCollector() {
		long antes = garbage.freeMemory();
		this.garbage.gc();
		long despues = garbage.freeMemory();
		return "Memoria libre pre limpieza: " + antes + ". Memoria libre Post Limpieza: " + despues;
	}

	public void muestroResultado(Set<Empleado> s) {
		s.toString();
		String[][] matriz = new String[s.size()][2];
		int i = 0;
		for (Empleado x : s) {
			matriz[i][0] = x.getNombre();
			matriz[i][1] = x.getRol().toString();
			i++;
		}

		ventana.muestroResultados(matriz);
	}

}
