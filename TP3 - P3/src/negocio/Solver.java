package negocio;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;

public class Solver
{
	private Grafo _grafo;
	private Set<Integer> _conjuntoActual;
	private Set<Integer> _cliqueActual;
	private ArrayList <Set<Integer>> _cliques;
	
	public Solver(Grafo grafo)
	{	_grafo = grafo;
		_cliques=new ArrayList<Set<Integer>>();
		resolver();
	}
	
	public Set<Integer> resolver()
	{
		_conjuntoActual = new HashSet<Integer>();
		_cliqueActual = new HashSet<Integer>();
		
		recursion(0);
		return _cliqueActual;
	}
	
	private void recursion(int inicial)
	{
		// Caso base
		if( inicial == _grafo.tamano())
		{
			if( conjuntoActualEsClique() ) {
				_cliqueActual = clonarConjuntoActual();
				_cliques.add(_cliqueActual);
			}
				
			return;
		}
		
		// Caso recursivo
		_conjuntoActual.add(inicial);
		recursion(inicial+1);
		
		_conjuntoActual.remove(inicial);
		recursion(inicial+1);
	}
	
	private Set<Integer> clonarConjuntoActual()
	{
		Set<Integer> ret = new HashSet<Integer>();
		for(Integer i: _conjuntoActual)
			ret.add(i);
		
		return ret;
	}

	private boolean conjuntoActualEsClique()
	{
		for(Integer i: _conjuntoActual)
		for(Integer j: _conjuntoActual) if( i != j && !_grafo.existeArista(i, j) )
			return false;
		
		return true;
	}
	
	public ArrayList<Set<Integer>> buscoCliques(){
		return _cliques;
	}
	
	
}















