package negocio;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;
import javax.swing.JButton;
import javax.swing.SwingWorker;

public class Proyecto extends SwingWorker<Set<Empleado>, Integer> {
	private NominaEmpleados nomina;
	private Necesidad necesidad;
	private Set<Empleado> equipo;
	private double cumplimiento;
	private Set<Integer> cliqueSolucion;
	private ArrayList<Set<Integer>> cliques;
	private JButton btnResultados;

	public Proyecto(NominaEmpleados nom, Necesidad nec) {
		nomina = nom;
		necesidad = nec;
		equipo = new HashSet<Empleado>();
		cliqueSolucion = new HashSet<Integer>();
		cumplimiento = 0.0;
	}

	@Override
	protected Set<Empleado> doInBackground() throws Exception {
		cliques = buscoTodasCliques(nomina.getGrafo());
		analizoCliques();
		agregoEmpleados();
		return equipo;
	}

	protected void analizoCliques() {
		int x = 0;
		while (!solcuionEsOptima() && x < cantidadCliques()) {
			if (puedeServir(cliques.get(x)))
				procesoClique(cliques.get(x));
			x++;
		}
	}

	private void procesoClique(Set<Integer> clique) {
		Necesidad n = copioNecesidad();
		Set<Integer> tmp = new HashSet<Integer>();
		clique.forEach(x -> {
			if (n.esNecesario(nomina.buscoPorId(x))) {
				n.descuento(nomina.buscoPorId(x));
				tmp.add(x);
			}
		});

		if (n.cumplimiento() > cumplimiento) {
			cliqueSolucion = clonarClique(tmp);
			cumplimiento = n.cumplimiento();
		}

	}

	private void agregoEmpleados() {
		cliqueSolucion.forEach(x -> agregarEmpleado(nomina.buscoPorId(x)));
	}

	private void agregarEmpleado(Empleado e) {
		equipo.add(e);
		necesidad.descuento(e);
	}

	private boolean puedeServir(Set<Integer> clique) {
		return (clique.size() * 1.0 / necesidad.getNecesidadTotal() * 1.0) > cumplimiento;
	}

	private ArrayList<Set<Integer>> buscoTodasCliques(Grafo g) {
		return new Solver(g).buscoCliques();
	}

	private Necesidad copioNecesidad() {
		return new Necesidad(necesidad.getLideres(), necesidad.getArquitectos(), necesidad.getProgramadores(),
				necesidad.getTesters());
	}

	private boolean solcuionEsOptima() {
		return cumplimiento == 1.0;
	}

	// escribi asi el metodo para facilitar legibilidad
	public void tengoEmpleadosPorPuesto() throws Exception {
		if (nomina.cantidadLideres() < necesidad.getLideres())
			throw new Exception("Faltan Lideres, La solucion no sera optima");
		if (nomina.cantidadArquitectos() < necesidad.getArquitectos())
			throw new Exception("Faltan Arquitectos, La solucion no sera optima");
		if (nomina.cantidadProgramadores() < necesidad.getProgramadores())
			throw new Exception("Faltan Programadores, La solucion no sera optima");
		if (nomina.cantidadTesters() < necesidad.getTesters())
			throw new Exception("Faltan Testers, La solucion no sera optima");
	}

	public int cantidadCliques() {
		return cliques.size();
	}

	public Set<Empleado> getSolucion() {
		return equipo;
	}

	private Set<Integer> clonarClique(Set<Integer> clique) {
		Set<Integer> ret = new HashSet<Integer>();
		for (Integer i : clique)
			ret.add(i);
		return ret;
	}

	public void setBtnResultados(JButton b) {
		btnResultados = b;
	}

	@Override
	protected void done() {
		btnResultados.setVisible(true);
	}
  
}
