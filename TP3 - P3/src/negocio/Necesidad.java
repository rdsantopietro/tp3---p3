package negocio;

public class Necesidad {

	private int lideres;
	private int arquitectos;
	private int programadores;
	private int testers;
	private int actualLideres;
	private int actualArquitectos;
	private int actualProgramadores;
	private int actualTesters;
	private int necesidadTotal;
	
	public Necesidad (int lideres, int arquitectos, int programadores, int testers) {
		this.lideres=lideres;
		this.arquitectos=arquitectos;
		this.programadores=programadores;
		this.testers=testers;
		actualArquitectos=0;
		actualLideres=0;
		actualProgramadores=0;
		actualTesters=0;
		necesidadTotal=lideres+arquitectos+programadores+testers;
	}
	
	public boolean esNecesario(Empleado e) {
		switch (e.getRol()) {
		case TESTER:
				return faltatTester();
		case LIDER:
			return faltaLider();
		case ARQUITECTO:
			return faltaArquitecto();
		case PROGRAMADOR:
			return faltaProgramador();
		default:
			return false;
		}
	}
	
	public boolean faltaLider() {
		boolean es=lideres>actualLideres;
		return es;
	}
	public boolean faltaArquitecto() {
		boolean es= arquitectos>actualArquitectos;
		return es;
	}
	public boolean faltaProgramador() {
		boolean es= programadores>actualProgramadores;
		return es;
	}
	public boolean faltatTester() {
		boolean es= testers>actualTesters;
		return es;
	}
	
	public void imprimoNecesidad() {
		System.out.println("NECESIDAD: Lideres:" + lideres +"/"+actualLideres+ ", arquitectos "+arquitectos+"/"+actualArquitectos
				+ "  Programadores " + programadores+ "/"+ actualProgramadores + " testers "+ testers+ "/"+actualTesters + " Cumplimiento: "+cumplimiento()) ;
	}

	public void descuento(Empleado e) {
		switch (e.getRol()) {
		case TESTER:
			actualTesters+=1;
			break;
		case LIDER:
			actualLideres+=1;
			break;
		case ARQUITECTO:
			actualArquitectos+=1;
			break;
		case PROGRAMADOR:
			actualProgramadores+=1;
			break;
		default:
			break;
		}
	}
	
	public int getNecesidadTotal() {
		return necesidadTotal;
	}
	
	public double cumplimiento() {
		return  (getEmpleadosActual()*1.0)/(getNecesidadTotal()*1.0);
	}

	public int getLideres() {
		return lideres;
	}

	public int getProgramadores() {
		return programadores;
	}

	public int getTesters() {
		return testers;
	}

	public int getArquitectos() {
		return arquitectos;
	}
	
	private int getEmpleadosActual() {
		return actualArquitectos+actualLideres+actualProgramadores+actualTesters;
	}
	
	
}
